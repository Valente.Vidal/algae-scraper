import json
import requests
import datetime 
import dateutil.relativedelta
import time
from pprint import pprint 
from settings_local import *






class Update_Json:
    """
    Updates the values.json file that the 
    prefill-and-scheduler/AutomatedScheduler uses to schedule
    runs
    """

    def get_past_date(self):
        today = datetime.datetime.now()
        three_m_ago = today - dateutil.relativedelta.relativedelta(months=3)
        #three_m_ago = three_m_ago.strftime("%Y_%m_%d")
        return (three_m_ago)

    def get_json(self):
        """
        get the json file which is a list of dictionaries
        """
        r = requests.get('http://urldb.empello.net/api/testurlstags/?format=json')
        return r.json()


    def get_recent_country_tags(self, country_tags_list):
        """
        This is limited to 3 months by get_past_date 
        """
        recent_country_tags = []
        for tag in country_tags_list:
            tag_date_s = tag[3:13]
            tag_date = datetime.datetime.strptime(tag_date_s, "%Y_%m_%d")
            #print (type(self.get_past_date)
            if tag_date > self.get_past_date():
                recent_country_tags.append(tag)
        return recent_country_tags



    def get_country_tags(self, proxy, tag_json):
        #print (proxy[:2])
        country_tags = []

        for tag in tag_json:
            if tag["name"][:2] == proxy[:2] and tag["name"][2:3] == "_": 
                # this is a check for the tag to have the 
                # desired format example:
                # uk_2019_06_25_MAINSTREAM
                country_tags.append(tag["name"])

        return country_tags


    def get_values(self):
        
        with open(VALUES_JSON_PATH, "r") as f:
            data = json.load(f)
        return data

    def save_values_json(self, new_values_json):
        with open(VALUES_JSON_PATH, 'w') as outfile:
            json.dump(new_values_json, outfile)



    def update_list(self):

        mena_exception = ["qa", "kw", "bh", "om", "iq", "ae", "eg", "ma", "sa"]
        # Mena has special requirements because of how sensitive running 
        # unknown urls in countries where porn is illegal 

        tag_json = self.get_json()
        values_dict = self.get_values()

        for proxy in values_dict:
            if proxy in mena_exception:
                pass
            else:
                country_tags_list = self.get_country_tags(proxy, tag_json)
                recent_country_tags = self.get_recent_country_tags(country_tags_list)
                values_dict[proxy]["listNumber"] = len(recent_country_tags)
                values_dict[proxy]["list"] = recent_country_tags
        

        self.save_values_json(values_dict)      
        print ("Json Values Updated!")

if __name__ == "__main__":
    Update_Json()
    

