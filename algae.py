from module_app_trends import App_trend_model as app_trends
from module_bbc_news import BBC_news as bbc_news
from module_twitter_trends import TwitterAPI as trends
from module_bing import Bing_results
from update_list_json import Update_Json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from available_countries import available_countries
from settings_local import *
from datetime import datetime
import requests
import sys
import argparse



class Algae:
    """
    Algae is a modular webscraper that collects keywords, searches using Bing.com
    and uploads the urls over to an api database, after its done, it updates the 
    tags created on the schedular. 
    """

    def chrome_driver(self):
        chromedriver_path = CHROMEDRIVER_PATH # in windows add .exe
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')  # Last I checked this was necessary.
        options.add_argument('--no-sandbox') #Also seems necessary
        prefs = {'profile.managed_default_content_settings.images':2}#so the browser doesnt load images
        options.add_experimental_option("prefs", prefs)#so the broswer doesn't load images
        driver = webdriver.Chrome(chromedriver_path, chrome_options=options)
        return driver


    def create_tag_name(self, country_code):
        date = f"{datetime.now():%Y_%m_%d}"
        country_code = country_code.lower()
        tag_name = f"{country_code}_{date}_MAINSTREAM"
        return tag_name

                      
    def search_tag(self, tag):
        """
        Checks if the tag exists on the database
        returns the id if it does or else it returns None
        """
        testurlstags = requests.get("http://urldb.empello.net/api/testurlstags.json").text
        testurlstags = eval(testurlstags)
        print (tag)
        for tag_dict in testurlstags:
            if tag_dict["name"] == tag:
                return tag_dict["id"]

    def create_tag(self, tag):
        r = requests.post("http://urldb.empello.net/api/testurlstags/", data={'name':tag})
        tag_id = r.json()["id"]
        return tag_id
                    
    def get_tag_id(self, tag):
        tag_id = self.search_tag(tag)
        if tag_id == None:
            tag_id = self.create_tag(tag)
            return tag_id
        return tag_id
                            
    ### ------------------------ api call ---------------------------------------

    def api_call(self, url, tag_id):
        
        category = "MAINSTREAM"
        r = requests.post("http://urldb.empello.net/api/testurls/", data={'url': url,
                                                            'category': category,
                                                            'tags': [tag_id]})


    ### ------------------------ update json values from schedular ---------------

    def update_json_values(self):
        print ("Updating List url ....")
        update_json = Update_Json()
        update_json.update_list()
    

#----------------- COODINATOR ---------------------------------------------------

    def coordinator (self, country_codes):

        country_codes = country_codes[2:]
          
        if country_codes == []:
            country_codes = available_countries()
            print ("Getting country codes from proxy DB")
            print ("-"*10)
            print(country_codes)

        print ("-"*10)
        print(country_codes)

        #stop = input("cancel") for dev

        driver = self.chrome_driver()


           
#----------------- Search for Keywords --------------------------------------------
        for country_code in country_codes:

            
            country_code = country_code.upper()


            keywords = []
           
            country_apps = app_trends(country_code)
            keywords.extend(country_apps.create_search_terms())

            country_news = bbc_news(country_code)
            keywords.extend(country_news.translated_headlines())

            country_trends = trends(country_code)
            keywords.extend(country_trends.trends())


#----------------- Search bing for each keyword -------------------------------------
            results = []
            for keyword in keywords:

                bing = Bing_results(driver, keyword, country_code)
                results.extend(bing.results_list())

#----------------- Upload every url to the UrlDB tagged with country code and date ---
            print ("Uploading urls to URLDB.... This might take a while")
            tag_id = self.get_tag_id(self.create_tag_name(country_code))
            for url in results:
                self.api_call(url, tag_id)
        driver.quit()

#----------- Update list ----------------------------------------

        
        self.update_json_values()
        print ("All Done")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--list", nargs="*" )
    args = parser.parse_args()

    a = Algae()
    a.coordinator(sys.argv)
    

