from bs4 import BeautifulSoup


def bing_messages(keyword, country_code, page_number):
        keyword = keyword.replace("+", " ")
        print ("-"*10)
        print ("Searching for: " + keyword)
        print ("Country Code: " + country_code)
        print ("Page number: " + str(page_number)[:1])


def get_links(page_source): #driver
    links = []
    soup = BeautifulSoup(page_source, "lxml")
    for link in soup.find_all("a"):
        try:
            if link["href"].startswith('http'):
                #print (link["href"]) # enable for troubleshoot purposes
                links.append(link["href"])
        except KeyError:
            pass
    return links


def bing_module(driver, keyword, country_code): #driver
    links_per_keyword = []
    limit = 20 #limit of pages, it jumps every 10, 50 being 5 pages
    keyword = keyword.replace(" ", "+")
    for page_number in range(0, limit, 10):
        url = f"https://www.bing.com/search?q={keyword}&first={page_number}&cc={country_code}"
        driver.get(url)
        bing_messages(keyword, country_code, page_number)
        links = get_links(driver.page_source)
        links_per_keyword.extend(links)
    
    return links_per_keyword




