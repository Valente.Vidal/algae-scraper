import requests
import json
from pprint import pprint 
import time 
from datetime import datetime

path = "/root/prefill/prefill-and-scheduler/AutomatedScheduler/"
def get_recent_tag(country_code, tag_list):
    date_list = []
    for date in tag_list:
        date_list.append(datetime.strptime(date, '%Y_%m_%d'))
    date = f"{max(date_list):%Y_%m_%d}"     
    latest_list = f"{country_code}_{date}_MAINSTREAM"
    return latest_list

def check_new_list(proxy):
    tag_list = []
    testurlstags = requests.get("http://urldb.empello.net/api/testurlstags.json").text
    testurlstags = eval(testurlstags)
    country_code = proxy[:2]
    print ("Country code: ", country_code)
    for url_tag in testurlstags:
        tag = url_tag["name"]
        if "MAINSTREAM" in tag:
            if country_code == tag[:2]:
                tag_list.append(tag[3:13])               
    recent_list = get_recent_tag(country_code, tag_list)
    return recent_list

new_values = {}
with open (path+"values.json") as json_file:
    data = json.load(json_file)
    
    for proxy, values in data.items():
        try:
            if values["adultList"] == "ADULT":
                try:                
                    latest_list = check_new_list(proxy)
                    old_list = values["list"]
                    values["list"] = [latest_list] + old_list
                    values["listNumber"] = len(values["list"])
                except ValueError:
                    pass
                
                new_values[proxy] = values
        except KeyError:
            pass
        new_values[proxy] = values
pprint(new_values)

with open(path+'values.json', 'w') as f:
    json.dump(new_values, f)


