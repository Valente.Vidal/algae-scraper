#For the twitter module you will need a consumer key and consumer secrete
#Both can be obtain by twitter by create a twitter app
# https://developer.twitter.com/en/apps

CONSUMER_KEY = 'KEY'
CONSUMER_SECRET = 'KEY'



#Add the chrome driver path in windows it should 
#include .exe

CHROMEDRIVER_PATH = "chromedriver.exe"



#This requires an api key from YANDEX
#Simple translate class using Yandex api https://tech.yandex.com/translate/doc/dg/reference/translate-docpage/#JSON
#api key can be obtained from the website currently using the one from 
#my email "x9...."

TRANSLATE_API_KEY = "API-KEY" 

#This is the path for the values json 
# that  prefill-and-scheduler/AutomatedScheduler uses to schedule runs

VALUES_JSON_PATH = "values.json"


#Country Json Path containing all of the countries information
COUNTRY_JSON_PATH = "country.json"

#Path for the woeid.json which contains the woeid from contries
WOEID_JSON_PATH = "woeid.json"

#Path for not_wanted_urls.txt

NOT_WANTED_URLS_PATH = "not_wanted_urls.txt"