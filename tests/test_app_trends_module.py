import unittest

import algae
from app_trends_module import *




class Test_App_Trend(unittest.TestCase):
    

    def test_android_trends(self):
        """
        Test that the function collects a list that is greater than 30 items
        """
        app_list = android_trends()
        
        self.assertTrue(len(app_list) >= 30)

    def test_ios_trends(self):
        """
        Test that the function collects a list that is greater than 30 items
        """
        app_list = ios_trends()
        self.assertTrue(len(app_list) >= 30)
    

    def test_app_trend_module(self):
        """
        Testing the whole module making sure it returns atleast 
        30 keywords per country
        """
        main_dictionary = {"UK": [], "IE": []}
        country_code = "IE"
        updated_dictionary = app_trend_module(country_code, main_dictionary)
        #print (updated_dictionary)
        self.assertTrue(len(updated_dictionary[country_code]) >= 30 )



    
if __name__ == '__main__':
    unittest.main()
