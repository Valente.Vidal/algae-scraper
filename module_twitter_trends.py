import requests
import base64
import json
from pprint import pprint
from settings_local import *




class TwitterAPI:
    """
    returns top trends from twitter based on the woeid codes
    """
    def __init__(self, country_code):
        self.consumer_key = CONSUMER_KEY
        self.consumer_secret = CONSUMER_SECRET
        self.country_code = country_code

    def bearer_token(self):
        encoded_CK = requests.utils.quote(self.consumer_key)
        encoded_CS = requests.utils.quote(self.consumer_secret)

        bearer_token  = ":".join((encoded_CK,encoded_CS))
        encoded_credentials = base64.b64encode(bearer_token.encode('utf-8'))

        headers = {'Authorization': 'Basic ' + encoded_credentials.decode('utf-8'),
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
        
        data = {'grant_type':'client_credentials'}
        r = requests.post("https://api.twitter.com/oauth2/token", data=data, headers=headers)
        return json.loads(r.text)['access_token']

    def country_name(self):
        with open(COUNTRY_JSON_PATH, 'r', encoding='utf8') as f:
            country_file = json.load(f)
            return country_file[self.country_code.upper()]["name"]

    def woeid(self):
        with open(WOEID_JSON_PATH, 'r', encoding='utf8') as f:
            woeid_list = json.loads(f.read())
            for i in woeid_list:
                if self.country_name() == i['name']:
                    return i['woeid']


    def trends_json(self):
        trends_list = []
        woeid = self.woeid()
        if woeid == None:
            print (f"Woeid code could not be found for {self.country_name()}, returning worldwide code '1' ")
            woeid = "1"
            
        headers = {'Authorization': 'Bearer ' + self.bearer_token()}
        r = requests.get('https://api.twitter.com/1.1/trends/place.json?id=' + str(woeid), headers=headers) 
        return json.loads(r.text)

    def trends(self):
        trends_list = []
        for trend in self.trends_json()[0]['trends']:
            trends_list.append(trend['name'])
        print ("Twitter trends for: " + self.country_name())
        return trends_list

# t = TwitterAPI("CA")
# print (t.trends())

if __name__ == "__main__":
    TwitterAPI()
    