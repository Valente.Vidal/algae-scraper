import requests
import ast


def available_countries():
    url = ("http://138.68.166.83/proxies/")
    r = requests.get(url)
    proxy_list = r.text
    proxy_list = proxy_list.replace("false", "False")
    proxy_list = proxy_list.replace("true", "True")
    proxy_list= eval(proxy_list)
    available_countries = []
    for proxy in proxy_list:
        if proxy['suspended'] == False:
            available_countries.append(proxy['country']) 
    available_countries = list(set(available_countries))
    return available_countries

if __name__ == '__main__':
    available_countries()
