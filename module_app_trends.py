# pylint: disable-all
import requests
from bs4 import BeautifulSoup
import json
import random
from translate_module import Translate as translate
import lxml
from settings_local import *

class App_trend_model:


    def __init__(self, country_code):
        """
        country_code is the two uppercase letter code (E.g UK, US, MX)
        """
        self.country_code = country_code
        #self.language_code = select_language()
        self.add_on_words = ["Download Free",
                            "Free Coins",
                            "Free Gems",
                            "Free Crystals",
                            "Free Health",
                            "Free Items",
                            "Free Coin",
                            "Free Silver",
                            "Free Unlock",]
#--------------------------------------verify input-------------------------------------
    @property
    def country_code(self):
        return self._country_code

    @country_code.setter
    def country_code(self, cc):
        if not isinstance(cc, str):
            raise TypeError ("country_code must be a two letter string (e.g. UK, US, MX)")
        self._country_code = cc
        if len(cc) != 2:
            raise Exception ("country_code must be a two letter string (e.g. UK, US, MX)")
        if not cc.isupper():
            raise Exception ("country_code must be a two letter uppercase string (e.g. UK, US, MX)")
#-------------------------------------------------------------------------------

    def get_ios_trends(self):
        """
        gets the top 100 free apps on the iOs store
        """
        ios_trends_list = []
        request = requests.get("https://www.apple.com/uk/itunes/charts/free-apps/")
        soup = BeautifulSoup(request.text, "lxml")
        for title in soup.find_all("h3"):
            try:
                ios_trends_list.append(title.text)
            except:
                raise Exception ("Error occurred while getting iOs apps")
        return ios_trends_list[:-8] #the last items in the list are apple related

    def get_android_trends(self):
        """
        get the 60 top android free games
        """
        android_trends_list = []
        request = requests.get("https://play.google.com/store/apps/collection/topselling_new_free")
        soup = BeautifulSoup(request.text, "lxml")
        for title in soup.find_all("a",{"class": "title"}):
            try:
                app = title["title"]
                #print (app)
                android_trends_list.append(app)
            except:
                raise Exception ("Error occurred while getting android apps")
        return android_trends_list


    def get_ios_android_trends(self):
        """
        get a list containing both ios and android lists
        """
        print("Getting list of apps from Android and iOs store")
        return  self.get_ios_trends() + self.get_android_trends()
        
#----------------------------------translate addons---------------------------------------------

    def get_language_code(self):
        """
        looks up what language is from the country code given
        """
        with open(COUNTRY_JSON_PATH, 'r', encoding='utf8') as f:
            country_file = json.load(f)    
        return (country_file[self.country_code]["languages"][0])


    def get_translate_add_on_words(self):
        translate_add_on_words = []
        translator = translate()
        for keywords in self.add_on_words:
            translated_keywords = translator.translate("en", self.get_language_code(), keywords )
            translate_add_on_words.append(translated_keywords)
        return translate_add_on_words

    def create_search_terms(self):
        """
        adds a random add-on word to the end of the end of the app name
        (e.g. Mr Bullet Free Coins ) in the language specified from the 
        country code
        """
        translated_add_on_list = self.get_translate_add_on_words()
        translated_search_terms = []
        for trend in self.get_ios_android_trends():             
            translated_search_terms.append(trend + " " + random.choice(translated_add_on_list))
        return translated_search_terms



# def main():

#     mx_app_trends = App_trend_model("MX")
#     print (mx_app_trends.create_search_terms())


if __name__ == "__main__":
    App_trend_model()